# LEA 

The aim of this software is to identify local adaptation or divergent selection through associating allele frequencies with an environmental gradient. 

### Installation

LEA is an R package and needs to be compiled in an R version (>= 3.3.0). Once the necessary R version is obtained you can install with the command below.

```r
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("LEA")
```

For Xanadu users:

```shell
# get the source code
wget https://bioconductor.org/packages/release/bioc/src/contrib/LEA_3.4.0.tar.gz
# load R
module load R/4.0.3
# unpack package and store it as a R library in your local directory
R CMD INSTALL --library=~/local/R_libs/ LEA_3.4.0.tar.gz
# load package through specifying destination
library("LEA", lib="~/local/R_libs/") 
```

### Ingredients

file formats are encoded by 0 (0 reference alleles), 1 (1 reference allele), 2 (both reference alleles), 9 (missing data) and describe per individual per loci. LEA offers functions that convert PED and/or VCF formats to both geno and lfmm. 

**geno**: 3 individuals and 4 genotypes. Required for Cross-validation statistics to determine number of latent factors.

```
010
012
019
012
```

**lfmm**: (space or tab seperated) 4 genotypes for 3 individuals. Need this for latent factor fixed model. 

```
1 0 0 1
1 1 9 2
2 0 1 1
```

**env**: 2 environmental variables for 3 individuals. 

```
0.252477 0.95250639
0.216618 0.10902647
-0.47509 0.07626694
```

### Population Structure (00:24:42)
Perform PCA and admixture analysis in LEA through pca() and snmf(). snmf() computes regularized least-squared estimates of the admixture coefficient. 

Determine a reasonable set of latent factors (K) to apply within the LFMM. This is based on finding the "knee point" in tracey widom percentages and respectively lower cross-validation scores.

```r
#!/bin/Rscript

## convert ped to geno and lfmm format
pruned_ped <- "../LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.pruned.RIND.ped"
pruned_geno <- ped2geno(pruned_ped, output.file="LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.pruned.RIND.geno",force=FALSE)
pruned_lfmm <- ped2lfmm(pruned_ped, output.file="LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.RIND.lfmm", force=FALSE)

## Determine number of Latent Factors (K)
# Compute PCA scores and obtain an object PCAProject
pc <- pca(lfmm)
# calculate tracy widom test
tw <- tracy.widom(pc)
# graph percentage of variance explained per significant component
PC_plot <- plot(tw$percentage)
ggsave("pc_plot.pdf", PC_plot, width = 15, height = 10, dpi = 50, limitsize = FALSE)

# create an snmf project for K = 1:15 (similar to STRUCTURE) alpha is a non-negative regularization parameter that penalizies intermediate values of ancestry coefficients 
obj.snmf <- snmf(pruned_geno, K = 1:15, entropy = T, ploidy = 2, project="new", alpha= 50, tolerance = 0.0001, repetitions = 5, iterations = 100000, CPU = 10)
# plot cross-validation error all K runs
CV_plot <- plot(obj.snmf, col = "blue4", cex = 1.4, pch = 13)
ggsave("cv_plotK1K18.pdf", CV_plot, width = 15, height = 10, dpi = 50, limitsize = FALSE)
```

<p align="center">
<img src=Rplots_PRUNED-1.png width = "45%">
<img src=Rplots_PRUNED-2.png width = "45%">
</p>
 
## Impute ()

Missing genotypes are solved for using probabilitie info from snmf(). We can select for 

It's advised to impute the dataset for missing genotypes by incorporating information from ancestral allele probabilities (K = #). Function will save recoded genotypes into input file name with an _imputed.lfmm extension.

```r
# Choose the run that minimizes the cross entropy from K = 5
best <- which.min(cross.entropy(obj.snmf, K = 5))
# method{mode,random}
impute(obj.snmf, "LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.lfmm", method = "mode", K = 5, run = best)
# read imputed lfmm dataset into environment or potentially call it when assigned to impute?
dat.imp <- read.lfmm("LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.lfmm_imputed.lfmm")
```

## Genome-Wide Association (Dependent on geno file size and # of runs + iterations)

<img align="center" width = 250 height = 65 src="https://render.githubusercontent.com/render/math?math=\G_{\i\ell} = \mu_\ell %2B\beta_{\ell}^{T}X_{i}%2B\U_{\i}^{T}V_{\ell}%2B\epsilon_{i\ell}">
_

```r
# Ecological Association Tests
grayling_project <- lfmm("LG.snps1.FFP5.MAF.01.HWE.01.MISS.65.MD.69.lfmm_imputed.lfmm", "grayling.env", K = 5:10, repetitions = 10, project = "new", iterations = 100000, burnin = 50000, CPU = 10, missing.data = FALSE, epsilon.noise = .001, epsilon.b = 1000)
```
For summary on the importance of iterations and burn-in read paragraph two. 
<p align="right">
<img src="MCMC_Box3.png" height = "450" width = "700">
</p>

## Post-Processing

To figure out which model complexity to acquire p-values from, we measure the genomic inflation factor and decide based on which model is closest to 1. As described in the paper larger K's results in lower power to reject nuetrality.

The inflation factor is dependent on deviation in median z-scores from the median of the chi-squared distribution.
<img align="center" width = 100 height = 85 src="https://render.githubusercontent.com/render/math?math=\z=\frac{x-\mu}{\sigma}">

p-values are thought to be correctely calibrated when the inflation factor is closest to 1. 

```r 

for (i in c(5,6,7,8,9)) {
        # store z-scores from i runs into an array
        zs.table <- z.scores(grayling_project,K=i)
        # Calculate the median
        zs <- apply(zs.table, MARGIN = 1, median)
        # Measure inflation factor (.456 = chi-squared distribution median) 
        lambda <- median(zs^2)/.456
        print(paste0("K = ",i," lambda = ",lambda))
}
```

### Combine z-scores from 10 runs

```r
# Fisher Stouffer methods to combine z-scores from multiple runs
pv <- lfmm.pvalues(grayling_project, K = 6)
EA_pvalues <- pv$pvalues
write.table(x = EA_pvalues, file = "LEA_EA_pvalues.txt",quote = F,row.names = F,col.names=F)
```

### Good choice of alpha

alpha is the probability of rejecting the null hypothesis when it is true, this is important for reducing the number of False-Positives.

```r
# Evaluate FDR and POWER
for (alpha in c(.05,.1,.15,.2)) {
  # expected FDR
  print(paste("expected FDR:", alpha))
  L = length(pv$pvalues)
  # Benjamini-Hochberg's method for an expected FDR = alpha. assumes the null p-value distribution be uniform
  w = which(sort(pv$pvalues) < alpha * (1:L)/L)
  candidates = order(pv$pvalues)[w]

  # estimated FDR and True Positive Rate
  # The targets SNPs are loci 351 to 400
  Lc = length(candidates)
  estimated.FDR = length(which(candidates <= 350))/Lc
  estimated.TPR = length(which(candidates > 350))/50
  print(paste("FDR:", estimated.FDR, "True Positive Rate:", round(estimated.TPR, digits = 2)))
}
```

## Results!!




